import React from 'react';
import { Link } from 'react-router-dom';
import {Col, Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';

export default function ProductCard({productProp}) {
	//Decsontruct the courseProp into their own variables
	const { _id, name, description, price } = productProp;


	return(
			<Col s={12} md={4} className="mb-3">
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						<Link  to={`/item/${_id}`}><Button variant="contained" endIcon={<ShoppingCartCheckoutIcon />}>View Product</Button></Link>	
					</Card.Body>
					
				</Card>
			</Col>

		)
}

//Check if the CourseCard component is getting the correct prop types
//PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.

ProductCard.propTypes = {
//shape() method - it is used to check if a prop object conforms to a specific 'shape'
	productProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}