import React from 'react';
import Button from '@mui/material/Button';
import Swal from 'sweetalert2';
import ArchiveIcon from '@mui/icons-material/Archive';
import UnarchiveIcon from '@mui/icons-material/Unarchive';

export default function ArchiveProduct({product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
		fetch(`https://boiling-citadel-51881.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}

	const unArchiveToggle = (productId) => {
		fetch(`https://boiling-citadel-51881.herokuapp.com/products/${productId}/product`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Err',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}
 

	return(
		<>
			{isActive ?

				<Button variant="contained" color="error"size="sm" onClick={() => archiveToggle(product)} endIcon={<ArchiveIcon />}>Archive</Button>

				:

				<Button variant="outlined" color="error" size="sm" onClick={() => unArchiveToggle(product)} endIcon={<UnarchiveIcon />}>Activate</Button>

			}
		</>

		)
}