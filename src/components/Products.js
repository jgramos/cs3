import React, {useContext, useState, useEffect} from 'react';
import UserView from '../components/Userview';
import UserContext from '../UserContext';

export default function Products() {

	const {user} =useContext(UserContext);
	console.log(user);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch('https://boiling-citadel-51881.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllProducts(data)
		})
	}


	useEffect(() => {
		fetchData()
	}, [])
	
	return (
		<UserView productsData={allProducts}/>

	)
}