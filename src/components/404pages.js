import React from 'react';
import { Link } from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';
  
export default function PageNotFound(){
    return(
    <Row>
        <Col>
            <div> 
              <h1 style={{ color: "red", fontSize: 100 }}>Oh No! Error 404</h1>
              <h2>Don't cry</h2>
              <Button variant="primary" as={Link} to="/">Back to home
              </Button>  
            </div>
            
        </Col>
    </Row>

        )

}