import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';

export default function Banner(){
	return(

		<Row>
			<Col className="p-5">
				<h1 className="mb-3">Never Stop</h1>
				<p className="my-3">Exploring The World</p>
				<Button variant="primary">About this Page</Button>
			</Col>
		</Row>

	)
}