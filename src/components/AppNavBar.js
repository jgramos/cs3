import React, {useState, useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
export default function AppNavbar() {

    const {user} = useContext(UserContext);
    
    return (
        <Navbar bg="dark" expand="lg" variant="dark">
            <Navbar.Brand className="ms-3"href="#home">Laman Dagat Store</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
                <Nav.Link as={Link} to="/">Home</Nav.Link>
                {(user.isAdmin === true) ?
                    <Nav.Link as={Link} to="/products">Dashboard</Nav.Link>
                    :
                    false
                }

                {(user.accessToken !== null) ?
                    <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                    :
                    <>
                    <Nav.Link as={Link} to="/login">Login</Nav.Link>
                    </>
                }

              </Nav>
            </Navbar.Collapse>
          </Navbar>

    )
}



