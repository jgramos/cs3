import React, { useState, useEffect } from 'react';
import { Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import CancelIcon from '@mui/icons-material/Cancel';

export default function EditProduct({product, fetchData}) {

	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [showEdit, setShowEdit] = useState(false)
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if(name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price])

	const openEdit = (productId) => {
		fetch(`https://boiling-citadel-51881.herokuapp.com/products/item/${ productId }`,{
			method:'GET',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('')
		setDescription('')
		setPrice(0)
	}

	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://boiling-citadel-51881.herokuapp.com/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Updated'
				})
				closeEdit();
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
				closeEdit();
				fetchData();
			}
		})
	}

	return(
		<>
			<Button variant="contained" size="sm" onClick={() => openEdit(product)}>Update</Button>

			{/*Edit Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button style={{margin: 10}} variant="contained" color="secondary"  endIcon={<CancelIcon />} onClick={closeEdit}>Cancel</Button>
					{isActive ?
						<Button variant="contained" color="success"  type="submit" endIcon={<SendIcon />}>Submit</Button>
						:
						<Button variant="contained" color="success"  type="submit" endIcon={<SendIcon />} disabled>Submit</Button>
					}
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>

		)
}