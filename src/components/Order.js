import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Specificproduct() {

	const navigate = useNavigate();

	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://boiling-citadel-51881.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	} ,[])

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')



	//enroll function
	const enroll = (productId) => {
		fetch('https://b165-shared-api.herokuapp.com/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Successfully Ordered',
					icon: 'success'
				})

				navigate('/')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}


	return (
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>

				<Card.Footer>

					{
                        user.accessToken !== null ?
                        <div className="d-grid gap-2">
                            <Button variant="primary" onClick={() => enroll(productId)}>
                                Order
                            </Button>
                        </div>
                        :
                        <Link className="btn btn-warning d-grid gap-2" to="/login">
                            Login to Order
                        </Link>
                    }
					
				</Card.Footer>
			</Card>
		</Container>

		)
}
