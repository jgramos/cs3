import React, { useState, useEffect } from 'react';
import { Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Button from '@mui/material/Button';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SendIcon from '@mui/icons-material/Send';
import CancelIcon from '@mui/icons-material/Cancel';


export default function AddProduct({fetchData}) {



	//Add state for the forms of product
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if(name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price])

	//States for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	//Functions to handle opening and closing of our AddProduct Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => {
		setShowAdd(false)
		setName("")
		setDescription("")
		setPrice("")
	}

	//Functions for fetching the create product in the backend
	const addProduct = (e) => {
		e.preventDefault();
		fetch('https://boiling-citadel-51881.herokuapp.com/products/create', {
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				//Show a success message
				Swal.fire({
					title:'Success',
					icon: 'success',
					text: 'Product succesfully added.'
				})
				//Close our modal
				closeAdd();
				//Render the updated data using the fetchData prop
				fetchData();
			} else{
				Swal.fire({
					title:'Something went wrong',
					icon:'error',
					text: 'Something went wrong. Please Try again'
				})
				closeAdd();
				fetchData();
			}

			//Clear out the input fields
			setName("")
			setDescription("")
			setPrice("")
		})
	}


	return(
		<>
			<Button variant="contained" color="success" onClick={openAdd} endIcon={<AddCircleOutlineIcon />}>Add New Product</Button>

		{/*Add Modal Forms*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" placeholder="Php 0.00" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button style={{margin: 10}} variant="contained" color="secondary"  endIcon={<CancelIcon />} onClick={closeAdd}>Cancel</Button>
					{isActive ?
						<Button variant="contained" color="success"  type="submit" endIcon={<SendIcon />}>Submit</Button>
						:
						<Button variant="contained" color="success"  type="submit" endIcon={<SendIcon />} disabled>Submit</Button>
					}
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>

		)
}



