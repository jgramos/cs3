import React, {useContext, useState, useEffect} from 'react';
import UserView from '../components/Userview';
import AdminView from '../components/Adminview';
import UserContext from '../UserContext';

export default function Dashboard() {

	const {user} =useContext(UserContext);
	console.log(user);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch('https://boiling-citadel-51881.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllProducts(data)
		})
	}



	useEffect(() => {
		fetchData()
	}, [])
	
	return (
		<>
			{(user.isAdmin === true) ?

			<AdminView productsData={allProducts} fetchData={fetchData}/>

			:

			<UserView productsData={allProducts}/>
			
			}

		</>
	)
}