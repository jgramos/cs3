import React, {useState, useEffect, useContext} from 'react';
import {Col, Row, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import {Link} from 'react-router-dom';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme();

export default function Login() {

	const navigate = useNavigate();

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function authenticate(e) {
		e.preventDefault();

		fetch('https://boiling-citadel-51881.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken});

				//Getting the user's credentials
				fetch('https://boiling-citadel-51881.herokuapp.com/users/all', {
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})
						navigate('/products')
					}else {
						navigate('/')
					}
				})

			} else{
				Swal.fire({
					title:'Ooops!',
					icon:'error',
					text:'Something Went Wrong. Check your Credentials'
				})
			}

			setEmail('');
			setPassword('');
		})
	}

	return(
		(user.accessToken !== null) ?
		<Navigate to="/products"/>
		:
		<ThemeProvider theme={theme}>
		<Container component="main" maxWidth="xs">
			<CssBaseline />
			<Box
	          sx={{
	            marginTop: 8,
	            display: 'flex',
	            flexDirection: 'column',
	            alignItems: 'center',
	          }}
	        >
	        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            	<LockOutlinedIcon />
	        </Avatar>
	        <Typography component="h1" variant="h5">
	        	Sign in
	        </Typography>

				<Form onSubmit={(e) => authenticate(e)}>
					<p>New user? <Link as={Link} to="/register">Create an account</Link></p>
					<Form.Group>
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter Email *"
							required
							value={email}
							onChange={e => setEmail(e.target.value)}
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Enter Password *"
							required
							value={password}
							onChange={e => setPassword(e.target.value)}
						/>
					</Form.Group>
					{isActive ?
						<Button variant="contained" type="submit" fullWidth sx={{ mt: 3, mb: 2 }}>Submit</Button>
						:
						<Button variant="contained" color="error" type="submit" fullWidth sx={{ mt: 3, mb: 2 }} disabled>Submit</Button>
					}
					
				</Form>
			</Box>
		</Container>
		</ThemeProvider>		
	)
}