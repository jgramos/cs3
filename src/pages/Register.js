import React, {useState, useEffect, useContext} from 'react';
import {Row, Col, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';
import {Link} from 'react-router-dom';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';


export default function Register() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2])

	function registerUser(e) {
		e.preventDefault();
		fetch('https://boiling-citadel-51881.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				setEmail('')
				setPassword1('')
				setPassword2('')

				Swal.fire({
					title: "Yaaaay!",
					icon:"success",
					text: "Thank you for registering!"
				});
				navigate('/');
			} else {
				Swal.fire({
					title: "Error!",
					icon:"err",
					text: "Email Already Exist!"
				});
			}			
		})	
	}

	return(
		<Container component="main" maxWidth="xs">
		<CssBaseline />
			<Box
	          sx={{
	            marginTop: 8,
	            display: 'flex',
	            flexDirection: 'column',
	            alignItems: 'center',
	          }}
	        >
	        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            	<LockOutlinedIcon />
	        </Avatar>
	        <Typography component="h1" variant="h5">
	        	Create an account
	        </Typography>

				<Form className="mt-2" onSubmit={(e) => registerUser(e)}>
					<p>Already have an account? <Link as={Link} to="/Login">Sign in</Link></p>
					<Form.Group>
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter Email *"
							required
							value={email}
							onChange={e => setEmail(e.target.value)}
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Enter Password *"
							required
							value={password1}
							onChange={e => setPassword1(e.target.value)}
						/>
					</Form.Group>

					<Form.Group sx={{ mt: 3, mb: 2 }}>
						<Form.Label >Verify Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Verify Password *"
							required
							value={password2}
							onChange={e => setPassword2(e.target.value)}
						/>
					</Form.Group>
					{isActive ?
						<Button variant="contained" type="submit" fullWidth sx={{ mt: 3, mb: 2 }}>Submit</Button>
						:
						<Button variant="contained" color="error" type="submit" fullWidth sx={{ mt: 3, mb: 2 }} disabled>Submit</Button>
					}
					
				</Form>
			</Box>
		</Container>
	)
}