import React, { useState, useEffect, useContext } from 'react';
import { Container, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import CancelIcon from '@mui/icons-material/Cancel';

export default function Specificproduct() {

  const navigate = useNavigate();

  //useParams() contains any values we are tryilng to pass in the URL stored
  //useParams is how we receive the productId passed via the URL
  const { productId } = useParams();

  useEffect(() => {

    fetch(`https://boiling-citadel-51881.herokuapp.com/products/item/${productId}`)
    .then(res => res.json())
    .then(data => {
      setName(data.name)
      setDescription(data.description)
      setPrice(data.price)
    })

  } ,[])

  const { user } = useContext(UserContext);

  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')

  const closeEdit = () => {
    navigate('/')
  }

  //enroll function
  const enroll = (productId) => {
    fetch('https://boiling-citadel-51881.herokuapp.com/orders/checkout', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      },
      body: JSON.stringify({
        productId: productId
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data) {
        Swal.fire({
          title: 'Successfully Ordered',
          icon: 'success'
        })

        navigate('/')
      }else {
        Swal.fire({
          title:'Something went wrong',
          icon: 'error'

        })
      }
    })
  }


  return (
    <Container>
      <Card>
        <Card.Header>
          <h4>{name}</h4>
        </Card.Header>

        <Card.Body>
          <Card.Text>{description}</Card.Text>
          <h6>Price: Php {price}</h6>
        </Card.Body>

        <Card.Footer>

          {
            user.accessToken !== null ?
            <div className="d-grid gap-2">
              <Button style={{margin: 10}} variant="contained" color="primary" onClick={() => enroll(productId)}>
                    Order
              </Button>
              <Button style={{margin: 10}} variant="contained" color="secondary"  endIcon={<CancelIcon />} onClick={closeEdit}>Cancel</Button>
                
            </div>
            :
            <Link className="d-grid" to="/login">
                <Button variant="contained" color="primary">Login to Order</Button>
            </Link>
          }
          
        </Card.Footer>
      </Card>
    </Container>

    )
}


