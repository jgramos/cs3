import React from 'react';

const UserContext = React.createContext();
//Creates a context object
//a context object as the same states is a data of an object that can be used to store information that can be shared to other components within the app.

export const UserProvider = UserContext.Provider;
export default UserContext;