import React,{useState} from 'react';
import './App.css';
import AppNavbar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import SignIn from './components/Sample';
import Footer from './components/Footer';
import Specificproduct from './pages/Specificproduct';
import Logout from './pages/Logout';
import PageNotFound from './components/404pages';
import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';

function App() {

    const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

    //Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear()
    }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <CssBaseline />
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />}/>
            <Route path="/login" element={<Login />}/>
            <Route path="/register" element={<Register />}/>       
            <Route path="/logout" element={<Logout />}/>
            <Route path="/products" element={<Dashboard />}/>
            <Route path="*" element={<PageNotFound />} />             
            <Route path="/SignIn" element={<Text />} />
            <Route path="/item/:productId" element={<Specificproduct />} />
          </Routes>  
        </Container>
        <Footer />
      </Router>
      
    </UserProvider>

      );
}


export default App;
